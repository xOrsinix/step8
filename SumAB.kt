import kotlinx.coroutines.*
import java.time.*


/*
    В случае последовательного вызова будет в 2 раза больше, чем при асинхронном, так как при последовательном вызове
    функции, возвращающие числа будут вызываться одна за другой, соотвественно, потребуется 800 миллисекунд, чтобы
    получить оба числа. В случае асинхронного вызова благодаря функции async корутины будут выполняться параллельно,
    будут созданны 2 Deferred объекта, которые при вызове методы await() будут возвращать числа.
    Таким образом на выполнение сложения потребуется только 400 миллисекунд
 */

suspend fun returnA():Int{
    delay(400L)
    return 5
}

suspend fun returnB():Int{
    delay(400L)
    return 8
}

suspend fun main()= coroutineScope{
    val clock:Clock=Clock.systemDefaultZone()
    var t1=clock.millis()
    println(returnA()+returnB())
    var t2= clock.millis()
    println("Time = ${t2-t1}")
    t1= clock.millis()
    val num1Def=async{ returnA() }
    val num2Def=async{ returnB() }
    println(num1Def.await()+num2Def.await())
    t2= clock.millis()
    println("Time = ${t2-t1}")
}