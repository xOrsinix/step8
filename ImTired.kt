import kotlinx.coroutines.*

fun main(args: Array<String>) = runBlocking<Unit> {
    val job =
        launch {
            try {
                repeat(3) { i ->
                    println("I'm sleeping $i")
                }
            } finally {
                async{
                    delay(400)
                    println("I'm running finally")
                }
            }
    }
    async{
        println("main: I'm tired of waiting!")
        delay(500)
        println("main: Now I can quit.")
    }
}